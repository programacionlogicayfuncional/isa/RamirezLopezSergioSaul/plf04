(ns plf02.core)

(defn stringE-1
  [x]
  (letfn [(f [x y]
            (if (empty? x)
              (if (and (> y 0) (< y 4))
                true false)
              (if (= \e (first x))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f x 0)))

(stringE-1 "Hello")
(stringE-1 "Heelle")
(stringE-1 "Heelele")
(stringE-1 "Hll")
(stringE-1 "e")
(stringE-1 "")

(defn stringE-2
  [x]
  (letfn [(f [y acc]
             (if(empty? y)
              (if (and (> acc 0) (< acc 4)) 
               true false)
               (if (identical? \e (first y))
                 (f (subs y 1) (inc acc))
                 (f (subs y 1) acc))))]
    (f x 0)))

(stringE-2 "Hello")
(stringE-2 "Heelle")
(stringE-2 "Heelele")
(stringE-2 "Hll")
(stringE-2 "e")
(stringE-2 "")


(defn string-times-1
  [x y]
  (letfn [(f [a b]
             (cond (= b 0) {:r (str a (f a (dec b)) :c "")}))]
   (f x y)))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)


(defn stringTimes-2
  [x y]
  (letfn [(f [a b acc]
            (if (zero? a)
              acc
              (f (dec a) b (str acc b))))]
    (f y x "")))

(stringTimes-2 "Hi" 2)
(stringTimes-2 "Hi" 3)
(stringTimes-2 "Hi" 1)
(stringTimes-2 "Hi" 0)
(stringTimes-2 "Hi" 5)
(stringTimes-2 "Oh Boy!" 2)
(stringTimes-2 "x" 4)
(stringTimes-2 "" 4)
(stringTimes-2 "code" 2)
(stringTimes-2 "code" 3)

(defn frontTimes-1
  [n s]
  (letfn [(f [x y]
            (if (zero? x)
              nil
              (str (subs y 0 3) (f (dec x) y))))]
    (f s n)))

(frontTimes-1 "Chocolate" 3)
(frontTimes-1 "Chocolate" 2)
(frontTimes-1 "Abcc" 3)
(frontTimes-1 "  Ab" 4)
(frontTimes-1 "   " 4)

(defn frontTimes-2 
  [n s]
  (letfn [(f [x y acc]
             (if (zero? x)
               acc
               (f (dec x) y (str acc (subs y 0 3)))))]
    (f s n "")))

(frontTimes-2 "Chocolate" 3)
(frontTimes-2 "Chocolate" 2)
(frontTimes-2 "Abc" 3)
(frontTimes-2 "  Ab" 4)
(frontTimes-2 "   " 4)

(defn countXX-1
   [x]
  (letfn [(f [a b]
            (if (empty? a)
              b
              (if (and (= \x (first a)) (= \x (first (rest a))))
                (f (rest a) (inc b))
                (f (rest a) b))))]
    (f x 0)))

(countXX-1 "abcxx")
(countXX-1 "xxx")
(countXX-1 "xxxx")
(countXX-1 "abc")
(countXX-1 "Hexxo thxxe")



(defn countXX-2
  [a]
  (letfn [(f [x acc]
             (if (empty? x)
               (if (> acc 1) (- acc 1) 0)
               (if (identical? \x (first x))
                 (f (subs x 1) (inc acc))
                 (f (subs x 1) acc))))]
    (f a 0)))

(countXX-2 "abcxx")
(countXX-2 "xxx")
(countXX-2 "xxxx")
(countXX-2 "Hexxo thxxe")

(defn stringSplosion-1
  [x]
  (letfn [(f [a b]
            (if (or (empty? a)
                    (>= b (count a)))
              ""
              (str (subs a 0 (+ b 1)) (f a (inc b)))))]
    (f x 0)))

(stringSplosion-1 "code")
(stringSplosion-1 "abc")
(stringSplosion-1 "ab")
(stringSplosion-1 "x")
(stringSplosion-1 "fade")
(stringSplosion-1 "There")
(stringSplosion-1 "Kitten")
(stringSplosion-1 "bay")
(stringSplosion-1 "Good")

(defn stringSplosion-2
  [x]
  (letfn [(f [a b acc]
            (if (or (empty? a)
                    (>= b (count a)))
              acc
              (f a (inc b) (str acc (subs a 0 (+ b 1))))))]
    (f x 0 "")))

(stringSplosion-2 "code")
(stringSplosion-2 "abc")
(stringSplosion-2 "ab")
(stringSplosion-2 "x")
(stringSplosion-2 "fade")
(stringSplosion-2 "There")
(stringSplosion-2 "Kitten")
(stringSplosion-2 "bay")
(stringSplosion-2 "Good")


(defn array123-1
  [bx]
  (letfn [(g [x]
             (>= (count x) 3)
             (h x))
          (h [xs]
             (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
             true)
          (f [xs]
             (if (empty? xs)
               false
               (g (rest xs))))]
    (g bx))) 

(array123-1 [1 1 2 3 1])
(array123-1 [1 1 2 4 1])
(array123-1 [1 1 2 1 2 3])
(array123-1 [1 1 2 1 2 1])
(array123-1 [1 2 3 1 2 3])
(array123-1 [1 2 3])
(array123-1 [1 1 1])
(array123-1 [1 2])
(array123-1 [1])
(array123-1 [])

(defn array123-2
  [bx]
  (letfn [(f [xs acc]
             (if (empty? xs)
               (str acc false)
               (if(>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (str acc true)
                  (f (rest xs) acc))
                 (f (empty xs) acc))))]
    (f bx "")))


(array123-2 [1 1 2 3 1])
(array123-2 [1 1 2 4 1])
(array123-2 [1 1 2 1 2 3])
(array123-2 [1 1 2 1 2 1])
(array123-2 [1 2 3 1 2 3])
(array123-2 [1 2 3])
(array123-2 [1 1 1])
(array123-2 [1 2])
(array123-2 [1])
(array123-2 [])


(defn stringX-1
  [x]
  (letfn [(f [a b]
            (if (empty? a)
              ""
              (if (== 0 b)
                (str (first a) (f (rest a) (inc b)))
                (if (and (= \x (first a)) (> (count a) 1))
                  (f (rest a) (inc b))
                  (str (first a) (f (rest a) (inc b)))))))]
    (f x 0)))

(stringX-1 "xxHxix")
(stringX-1 "abxxxcd")
(stringX-1 "xabxxxcdx")
(stringX-1 "xKittenx")
(stringX-1 "Hello")
(stringX-1 "xx")
(stringX-1 "x")
(stringX-1 "")

(defn stringX-2
   [x]
(letfn [(f [x y acc]
          (if (empty? x)
            acc
            (if (== 0 y)
              (str (first x) (f (rest x) (inc y) (str acc)))
              (if (and (= \x (first x)) (> (count x) 1))
                (f (rest x) (inc y) (str acc))
                (str (first x) (f (rest x) (inc y) acc))))))] 
  (f x 0 ""))) 

(stringX-2 "xxHxix")
(stringX-2 "abxxxcd")
(stringX-2 "xabxxxcdx")
(stringX-2 "xKittenx")
(stringX-2 "Hello")
(stringX-2 "xx")
(stringX-2 "x")
(stringX-2 "")

(defn altPairs-1
  [a]
  (letfn [(f [x i]
             (if(== (count x) i)
              ""
               (if (or (== i 0) (== i 1) (== i 4) (== i 5) (== i 8) (== i 9))
                 (str (subs x i (+ i 1)) (f x (inc i)))
                 (f x (inc i)))))]
    (f a 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
 [a]
  (letfn [(f [x i acc]
             (if(== (count x) i)
              acc
               (if (or (== i 0) (== i 1) (== i 4) (== i 5) (== i 8) (== i 9))
                 (f x (inc i) (str acc (subs x i (+ i 1))))
                 (f x (inc i) acc))))]
    (f a 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOther")

(defn stringYak-1
  [x]
  (letfn [(f [a b]
            (if (empty? a)
              ""
              (if (and (= (first a) \y) (= (first (rest a)) \a) (= (first (rest (rest a))) \k) (> (count a) 1))
                (f (rest (rest (rest a))) (inc b))
                (str (first a) (f (rest a) (inc b))))))]
    (f x 0)))

(stringYak-1 "tareaplf")
(stringYak-1 "yakpak")
(stringYak-1 "pakyak")
(stringYak-1 "yak123ya")
(stringYak-1 "yak")
(stringYak-1 "yakxxxyak")
(stringYak-1 "HiyakHi")
(stringYak-1 "xxxyakyyyakzzz")

(defn stringYak-2
  [x]
  (letfn [(f [a b acc]
            (if (empty? a)
              acc
              (if (and (= (first a) \y) (= (first (rest a)) \a) (= (first (rest (rest a))) \k) (> (count a) 1))
                (f (rest (rest (rest a))) (inc b) acc)
                (f (rest a) (inc b) (str acc (first a))))))]
    (f x 0 "")))

(stringYak-2 "tareayakplfyak")
(stringYak-2 "yakpak")
(stringYak-2 "pakyak")
(stringYak-2 "yak123ya")
(stringYak-2 "yak")
(stringYak-2 "yakxxxyak")
(stringYak-2 "HiyakHi")
(stringYak-2 "xxxyakyyyakzzz")

(defn array271-1
  [bx]
  (letfn [(f [xs]
            (if (empty? xs)
              false
              (if (>= (count xs) 3)

                (if (and (or (== (first xs) 2) (== (first xs) 1)) (== (first (rest xs)) 7) (or (== (first (rest (rest xs))) 1) (== (first (rest (rest xs))) 5) (== (first (rest (rest xs))) 3) (== (first (rest (rest xs))) -2)))
                 true
                  (f (rest xs)))
                false)))]
    (f bx)))

(array271-1 [1 2 7 1])
(array271-1 [1 2 8 1])
(array271-1 [2 7 1])
(array271-1 [3 8 2])
(array271-1 [2 7 4])
(array271-1 [2 7 -1])
(array271-1 [4 5 3 8 0])
(array271-1 [2 7 -2])
(array271-1 [2 7 5 10 4])
(array271-1 [2 7 -2 4 9 3])
(array271-1 [2 7 5 10 1])
(array271-1 [1 1 4 9 0])
(array271-1 [1 1 4 9 4 9 2])

(defn array271-2
  [bx]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (str acc false)
              (if (>= (count xs) 3)
                (if (and (or (== (first xs) 2) (== (first xs) 1)) (== (first (rest xs)) 7) (or (== (first (rest (rest xs))) 1) (== (first (rest (rest xs))) 5) (== (first (rest (rest xs))) 3) (== (first (rest (rest xs))) -2)))
                  (str acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f bx "")))

(array271-2 [1 2 7 1])
(array271-2 [1 2 8 1])
(array271-2 [2 7 1])
(array271-2 [3 8 2])
(array271-2 [2 7 4])
(array271-2 [2 7 3])
(array271-2 [2 7 -1])
(array271-2 [2 7 -2])
(array271-2 [4 5 3 8 0])
(array271-2 [2 7 5 10 4])
(array271-2 [2 7 -2 4 9 3])
(array271-2 [2 7 5 10 1])
(array271-2 [1 1 4 9 0])
(array271-2 [1 1 4 9 4 9 2])
